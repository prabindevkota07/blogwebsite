from rest_framework.serializers import ModelSerializer
from django.apps import apps
Post = apps.get_model('user', 'Post')
Comment = apps.get_model('user', 'Comment')


class PostSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'
        depth = 1

class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'


