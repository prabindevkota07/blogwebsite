from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .serializers import PostSerializer,CommentSerializer
from django.apps import apps
Post = apps.get_model('user', 'Post')
Comment = apps.get_model('user', 'Comment')


# Create your views here.


class PostViewSet(ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class CommentViewSet(ModelViewSet):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
