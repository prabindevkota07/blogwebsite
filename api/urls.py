from rest_framework.routers import DefaultRouter
from django.urls import path, include
from .views import PostViewSet, CommentViewSet


router = DefaultRouter()
router.register('post', PostViewSet)
router.register('comment', CommentViewSet)

urlpatterns = [

    path('', include(router.urls))
]