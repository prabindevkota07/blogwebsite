import json

from django.http import JsonResponse
from django.views.generic.detail import SingleObjectMixin
from django.forms.models import model_to_dict
from django.core import serializers
from .forms import PostForm, CommentForm
from .models import Post, Comment
from django.contrib import messages
from django.shortcuts import render, HttpResponse, get_object_or_404, redirect
from django.views import generic
from django.views.generic import View, FormView, RedirectView, ListView, CreateView, DetailView, UpdateView, DeleteView
from django.urls import reverse_lazy


# Create your views here.
class PostListView(ListView):
    model = Post
    template_name = 'list.html'


class PostCreateView(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'create.html'
    success_url = reverse_lazy('list')


class PostDetailView(DetailView):
    model = Post
    template_name = 'detail.html'
    context_object_name = 'post'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        comments_connected = Comment.objects.filter(
            blogpost_connected=self.get_object()).order_by('date_posted')
        data['comments'] = comments_connected
        data['form'] = CommentForm

        return data

    def post(self, request, *args, **kwargs):
        new_comment = Comment(content=request.POST.get('content'),
                              author=request.POST.get('author'),
                              blogpost_connected=self.get_object())
        new_comment.save()
        comments_connected = Comment.objects.filter(
            blogpost_connected=self.get_object()).order_by('date_posted').values('content', 'author', 'blogpost_connected','date_posted', 'id')
        comments= json.dumps(list(comments_connected), default=str)
       # ser_instance = serializers.serialize('json', comments)  # list of dictionary with necessary info
        #return JsonResponse({'instance': ser_instance})

        return JsonResponse({'status':'save', 'comments': comments})
       # return self.get(self, request, *args, **kwargs)


class PostUpdateView(UpdateView):
    model = Post
    template_name = 'update.html'
    form_class = PostForm
    context_object_name = 'post'
    success_url = reverse_lazy('index')


class PostDeleteView(DeleteView):
    model = Post
    template_name = 'delete.html'
    context_object_name = 'post'
    success_url = reverse_lazy('index')
