from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views
from .views import PostCreateView,  PostDeleteView, PostUpdateView, PostListView, PostDetailView

urlpatterns = [
       # path('', PostListView.as_view(), name='list'),
       path('delete/<int:pk>', PostDeleteView.as_view() , name='delete'),
       path('update/<int:pk>',  PostUpdateView.as_view(), name='update'),
       path('list/', PostListView.as_view(), name='list'),
       path('create/', PostCreateView.as_view(), name='create'),
       path('detail/<int:pk>', PostDetailView.as_view(), name='detail')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
