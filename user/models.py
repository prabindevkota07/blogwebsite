from django.db import models
from django.utils import timezone


# Create your models here.


class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    image = models.ImageField(upload_to='templates/img')
    image1 = models.ImageField(upload_to='templates/img')
    image2 = models.ImageField(upload_to='templates/img')
    created_date = models.DateTimeField(
        default=timezone.now)

    def __str__(self):
        return self.title


class Comment(models.Model):
    blogpost_connected = models.ForeignKey(
        Post, related_name='comments', on_delete=models.CASCADE)
    author = models.CharField(max_length=200)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.blogpost_connected.title[:40]


